### What is this repository for? ###
This repository contains Scorm Packages used in the development, testing and demos for Scorm.Run

### Usage ###
DevAccelerator hereby grants you a non-exclusive, non-transferable license to use these packages as-is, with attribution, for any non-commercial puropse.
Just to be clear, you can the packages in any way you find fit, but you are not allowed to sell them.
